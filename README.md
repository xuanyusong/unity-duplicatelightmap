unity4.x 美术烘培完场景后， 一些不需要烘培影子的物体，直接Ctrl+D拷贝复出出来一份即可，不需要重新烘培。


可是unity5 就不是这样了，如下图所示，unity5中Ctrl+D后 Lightmap丢失了

如下图所示

![Snip20160525_1.png](https://bitbucket.org/repo/ra8Xz9/images/2447091597-Snip20160525_1.png)

如果想复制出来一份，必须要重新烘培才行。我们的场景非常的大，烘培一次要4个小时，每次拷贝出来一份都烘培一遍的话美术要发疯了！！

开始我的想法是监听 Ctrl + D的接口，程序监听 复制操作， 直接把原MeshRender 上的 lightmapIndex 和 lightmapScaleOffset 复制给新拷贝出来的的。

监听Ctrl +D 的方法：

![8b0d88d5jw1f4202oe3pgj20gl0blwfp.jpg](https://bitbucket.org/repo/ra8Xz9/images/876954878-8b0d88d5jw1f4202oe3pgj20gl0blwfp.jpg)

但是直接监听Ctrl+D 似乎有点太霸道了， 美术并不希望所有gameObject 执行Ctrl + D 都要赋值LightMap。 所以最后干脆就新加一个快捷键
Ctrl+ shift + D


操作步骤
1.选择一个需要复制Lightmap信息的gameObject （支持多选）

![Snip20160525_2.png](https://bitbucket.org/repo/ra8Xz9/images/564298549-Snip20160525_2.png)

2.按下快捷键 Ctrl + Shirt + D （Mac 下是 Ctrl 是 Command）  
如下图所示， 已经复制出来了。 LightMap信息也是对的。

![Snip20160525_3.png](https://bitbucket.org/repo/ra8Xz9/images/1605493658-Snip20160525_3.png)


遇到的坑
lightmapIndex 和 lightmapScaleOffset 的赋值操作必须在Awake()方法里， 如果写到Start方法里 可能会出现 LightMap变花的情况。

工程直接下载下来就能用， 欢迎大家测试。

测试环境
unity5.3.3
小米pad1